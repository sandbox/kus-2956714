<?php

namespace Drupal\freeridesitemap\Controller;

use Drupal\Core\Controller\ControllerBase;

class freeridesitemapController extends ControllerBase
{

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    return array(
      '#type' => 'markup',
        '#markup' => $this->t('Welcome to a new and improved site map controller!'),
    );
  }

}