<?php

namespace Drupal\freeridesitemap\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'freeridesitemap' Block.
 *
 * @Block(
 *   id = "freeridesitemap_Block",
 *   admin_label = @Translation("freeridesitemap Block"),
 *   subject = @Translation("freeridesitemap Block"),
 * )
 */
class freeridesitemapBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build= array(
      '#type' => 'markup',
      '#markup' => '<h1>Hello</h1>',
    	);
    return $build;
  }

}