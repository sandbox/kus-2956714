<?php
declare(strict_types=1);

namespace Drupal\freeridesitemap\Entity;

class freeridesitemapEntity extends \Drupal\Core\Config\Entity\ConfigEntityBase implements \Drupal\freeridesitemap\SiteMapEntityInterface
{

  /** @var int */
  private $identifier;

  /** @var string */
  private $title;

  /** @var string */
  private $address;

  /** @var \DateTime */
  private $lastUpdated;

  public function getIdentifier() {
    return $this->identifier;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getAddress() {
    return $this->address;
  }

  public function getLastUpdated() {
    return $this->lastUpdated;
  }
}